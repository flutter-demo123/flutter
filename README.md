--TOPICS COVERED
********************************************************************************************************************
FLUTTER TOAST

FIREBASE(register,login,logout,database,storage)

NAVIGATION DRAWER

SPLASH SCREEN

LIST VIEW

DROPDOWN LIST

IMAGE PICKER
**********************************************************************************************************************



--DEPENDENCIES

(for firebase)

(1)place firebase config json file in android/app

(2)in android/app/build.gradle 

     (a)change minSdkVersion to 21, 
 
     (b)add apply plugin: 'com.google.gms.google-services' (place at bottom)

(3)in android/build.gradle

     (a)add classpath 'com.google.gms:google-services:4.3.4' (in dependencies)

(4)replace settings.gradle with
**************************************************************************************
include ':app'

def flutterProjectRoot = rootProject.projectDir.parentFile.toPath()

def plugins = new Properties()
def pluginsFile = new File(flutterProjectRoot.toFile(), '.flutter-plugins')
if (pluginsFile.exists()) {
    pluginsFile.withReader('UTF-8') { reader -> plugins.load(reader) }
}

plugins.each { name, path ->
    def pluginDirectory = flutterProjectRoot.resolve(path).resolve('android').toFile()
    include ":$name"
    project(":$name").projectDir = pluginDirectory
*****************************************************************************************




(5)in pubspec.yaml add dependencies
   
     firebase_core: ^0.5.0+1

    firebase_database: ^4.1.1

    firebase_auth: ^0.18.1+2

    flutter_signin_button: ^1.0.0

    #storage

    cloud_firestore: ^0.14.1+3

    firebase_storage: ^4.0.1




--ISSUES

(1)if db permission is denied auth/signin method/anonymous/enabled

(2)db rules read and write true